package com.org.intermedia.java.transport.services;

import java.sql.Connection;

import com.org.intermedia.java.transport.dao.LocationsDao;
import com.org.intermedia.java.transport.dao.UserDao;
import com.org.intermedia.java.transport.payloads.LocationPayload;
import com.org.intermedia.java.transport.payloads.UserPayload;
import com.org.intermedia.java.transport.response.BaseResponse;
import com.org.intermedia.java.transport.response.LocationResponse;
import com.org.intermedia.java.transport.response.UserResponse;
import com.org.intermedia.java.transport.utils.Constants;

public class UserService {
	
	public UserResponse createUser(UserPayload payload) throws Exception{
		if(checkPayload(payload)) {
			BaseResponse baseResponse = new BaseResponse();
			UserResponse userResponse = new UserResponse();
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			userResponse.setBaseResponse(baseResponse);
			return userResponse;
		}
		Connection connection = getConnection();
		UserDao userDao = new UserDao();
		return userDao.createUser(payload, connection);
	}
	private Connection getConnection() throws Exception {
		DbConnection dbConnection=new DbConnection();
		Connection connection = dbConnection.getConnection();
		return connection;
	}
	public UserResponse getUser(UserPayload payload) throws Exception{
		Connection connection = getConnection();
		UserDao userDao = new UserDao();
		return userDao.authUser(payload, connection);
	}
	public boolean checkPayload(UserPayload payload) {
		if(payload == null || payload.getUser() == null) {
			return true;
		}else if(payload.getUser().getFirstName() == null || 
				 payload.getUser().getLastName() == null ||
				 payload.getUser().getUserEmail() == null) {
			return true;
		}else if(payload.getUser().getFirstName().equals("") || 
				 payload.getUser().getLastName().equals("") ||
				 payload.getUser().getUserEmail().equals("")) {
			return true;
		}
		return false;
	}
	
	
}
