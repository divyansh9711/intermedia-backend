package com.org.intermedia.java.transport.tables;

public class RouteDB {

	private static String tableName = "route";
	private static String routeId = "routeId";
	private static String title = "title";
	private static String info = "info";
	private static String startLocationId = "startLocationId";
	private static String endLocationId = "endLocationId";
	private static String fare = "fare";
	private static String interval = "intervel";
	public static String getTableName() {
		return tableName;
	}
	public static void setTableName(String tableName) {
		RouteDB.tableName = tableName;
	}
	public static String getRouteId() {
		return routeId;
	}
	public static void setRouteId(String routeId) {
		RouteDB.routeId = routeId;
	}
	public static String getTitle() {
		return title;
	}
	public static void setTitle(String title) {
		RouteDB.title = title;
	}
	public static String getInfo() {
		return info;
	}
	public static void setInfo(String info) {
		RouteDB.info = info;
	}
	public static String getStartLocationId() {
		return startLocationId;
	}
	public static void setStartLocationId(String startLocationId) {
		RouteDB.startLocationId = startLocationId;
	}
	public static String getEndLocationId() {
		return endLocationId;
	}
	public static void setEndLocationId(String endLocationId) {
		RouteDB.endLocationId = endLocationId;
	}
	public static String getFare() {
		return fare;
	}
	public static void setFare(String fare) {
		RouteDB.fare = fare;
	}
	public static String getInterval() {
		return interval;
	}
	public static void setInterval(String interval) {
		RouteDB.interval = interval;
	}
	
	
}