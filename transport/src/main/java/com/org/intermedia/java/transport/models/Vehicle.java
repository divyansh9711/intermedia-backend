package com.org.intermedia.java.transport.models;

import java.util.ArrayList;

public class Vehicle extends Profile {

	private String vehicleName;
	private String vehicleRegNum;
	private Category vehicleType;
	private User owner;
	private User driver;
	private Route route;
	private Location startLocation;
	private Location endLocation;
	private Timing timing;
	public ArrayList<Route> sub;
	
	
	
	public ArrayList<Route> getSub() {
		return sub;
	}
	public void setSub(ArrayList<Route> sub) {
		this.sub = sub;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public User getDriver() {
		return driver;
	}
	public void setDriver(User driver) {
		this.driver = driver;
	}
	public Route getRoute() {
		return route;
	}
	public void setRoute(Route route) {
		this.route = route;
	}
	public Location getStartLocation() {
		return startLocation;
	}
	public void setStartLocation(Location startLocation) {
		this.startLocation = startLocation;
	}
	public Location getEndLocation() {
		return endLocation;
	}
	public void setEndLocation(Location endLocation) {
		this.endLocation = endLocation;
	}
	public Timing getTiming() {
		return timing;
	}
	public void setTiming(Timing timing) {
		this.timing = timing;
	}
	public String getVehicleName() {
		return vehicleName;
	}
	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}
	public String getVehicleRegNum() {
		return vehicleRegNum;
	}
	public void setVehicleRegNum(String vehicleRegNum) {
		this.vehicleRegNum = vehicleRegNum;
	}
	public Category getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(Category vehicleType) {
		this.vehicleType = vehicleType;
	}
	
	
}