package com.org.intermedia.java.transport.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.org.intermedia.java.transport.models.Category;
import com.org.intermedia.java.transport.models.Location;
import com.org.intermedia.java.transport.models.Route;
import com.org.intermedia.java.transport.models.Timing;
import com.org.intermedia.java.transport.payloads.RoutePayload;
import com.org.intermedia.java.transport.response.BaseResponse;
import com.org.intermedia.java.transport.response.RouteResponse;
import com.org.intermedia.java.transport.tables.RouteDB;
import com.org.intermedia.java.transport.tables.RouteRelationDB;
import com.org.intermedia.java.transport.utils.Constants;

public class RouteDao {

	public RouteResponse createRoute(RoutePayload payload, Connection conn) {

		Route route = payload.getRoute();
		BaseResponse baseResponse = new BaseResponse();
		
		PreparedStatement roleQuery = null,subRoleQuery = null;
		ResultSet genratedTuple;
		int result = -99,subResult = - 99;
		RouteResponse routeResponse = new RouteResponse();
		routeResponse.setBaseResponse(baseResponse);
		routeResponse.setRoute(route);
		boolean status = true;
		
		try {
			String query = "INSERT into"
					       + Constants.tableRoute
					       + "(" + RouteDB.getTitle()
					       + "," + RouteDB.getInfo()
					       + "," + RouteDB.getStartLocationId()
					       + "," + RouteDB.getEndLocationId()
					       + "," + RouteDB.getFare()
					       + "," + RouteDB.getInterval() + ")"
					       + " values "
					       + "('" + route.getTitle()
					       + "','" + route.getInfo()
					       + "','" + Integer.parseInt(route.getStartLocation().getuId())
					       + "','" + Integer.parseInt(route.getEndLocation().getuId())
					       + "','" + route.getFare()
					       + "','" + route.getInterval().getStartTime() +"')";
			
			System.out.println(query);
			roleQuery = conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			result = roleQuery.executeUpdate();
			RouteRelationDao routeRelationDao = new RouteRelationDao();
			if(result>0){
				genratedTuple = roleQuery.getGeneratedKeys();
				while (genratedTuple.next()) {
					route.setuId(genratedTuple.getString(1));
				}
				status = routeRelationDao.createRoute(route, conn);
			}
			
		}catch(Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		if(status) {
			baseResponse.setStatusCode(Constants.SU200);
			baseResponse.setMessage(Constants.LOCATION_INSERTED_SUCESSFULLY);
			routeResponse.setBaseResponse(baseResponse);
			routeResponse.setRoute(route);
		}else {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			routeResponse.setBaseResponse(baseResponse);
		}
		return routeResponse;
	}
	
	public RouteResponse getRoute(RoutePayload payload,Connection conn) {
		BaseResponse baseResponse = new BaseResponse();
		String routeId = payload.getId();
		RouteResponse routeResponse = new RouteResponse();
		PreparedStatement roleQuery = null;
		ResultSet result = null;
		Route route = new Route();
		boolean status = true;
		try {
			String query = "select * from " + Constants.tableRoute + " where " + RouteDB.getRouteId() + " = " + routeId;
			//System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			
			while(result.next()) {
				route.setuId(routeId);
				route.setTitle(result.getString(RouteDB.getTitle()));
				route.setInfo(result.getString(RouteDB.getInfo()));
				route.setFare(result.getString(RouteDB.getFare()));
				Location location = new LocationsDao().getLocation(result.getString(RouteDB.getStartLocationId()), conn);
				route.setStartLocation(location);
				location = new LocationsDao().getLocation(result.getString(RouteDB.getEndLocationId()), conn);
				route.setEndLocation(location);
				Timing timing = new Timing(result.getString(RouteDB.getInterval()),"","");
				route.setInterval(timing);
				route.setSubRoutes(new RouteRelationDao().getSubRoutes(routeId, conn));
			}
			routeResponse.setRoute(route);
		}catch(SQLException e) {
			System.out.println(e.toString());
			status = false;
		}catch(Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		if(status) {
			baseResponse.setStatusCode(Constants.SU200);
			baseResponse.setMessage(Constants.LOCATION_INSERTED_SUCESSFULLY);
			routeResponse.setBaseResponse(baseResponse);
			routeResponse.setRoute(route);
		}else {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			routeResponse.setBaseResponse(baseResponse);
		}
		return routeResponse;
		
	}
	
	public Route getRoute(String routeId,Connection conn) {

		PreparedStatement roleQuery = null;
		ResultSet result = null;
		Route route = new Route();
		try {
			String query = "select * from " + Constants.tableRoute + " where " + RouteDB.getRouteId() + " = " + routeId;
			//System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			
			while(result.next()) {
				route.setuId(routeId);
				route.setTitle(result.getString(RouteDB.getTitle()));
				route.setInfo(result.getString(RouteDB.getInfo()));
				route.setFare(result.getString(RouteDB.getFare()));
				Location location = new LocationsDao().getLocation(result.getString(RouteDB.getStartLocationId()), conn);
				route.setStartLocation(location);
				location = new LocationsDao().getLocation(result.getString(RouteDB.getEndLocationId()), conn);
				route.setEndLocation(location);
				Timing timing = new Timing(result.getString(RouteDB.getInterval()),"","");
				route.setInterval(timing);
				ArrayList<Route> subRoutes = new ArrayList<>();
				//subRoutes.addAll(new RouteRelationDao().getSubRoutes(routeId, conn));
				//System.out.println(subRoutes.size() + "\n\n\n\n\n\n\n\n\n\n\n");
				route.setSubRoutes(subRoutes);
			}
		}catch(SQLException e) {
			System.out.println(e.toString());
			
		}catch(Exception e) {
			System.out.println(e.toString());
			
		}
		return route;
	}
	public String getRoute(String startLocationId,String endLocationId,Connection conn) {

		PreparedStatement roleQuery = null;
		ResultSet result = null;
		String routeId = "-99";
		try {
			String query = "select * from " + Constants.tableRoute + " where " 
							+ RouteDB.getStartLocationId() + " = " + startLocationId 
							+ " and " + RouteDB.getEndLocationId() + " = " + endLocationId;
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			
			while(result.next()) {
				routeId = result.getString(RouteDB.getRouteId());
				}
		}catch(SQLException e) {
			System.out.println(e.toString());
			
		}catch(Exception e) {
			System.out.println(e.toString());
			
		}
		return routeId;
	}
	public String createRoute(String startLocationId,String endLocationId, Connection conn) {

		
		PreparedStatement roleQuery = null;
		ResultSet genratedTuple;
		int result = -99;
		String routeId = "-99";
		
		try {
			String query = "INSERT into"
					       + Constants.tableRoute
					       + "(" + RouteDB.getStartLocationId()
					       + "," + RouteDB.getEndLocationId() +  ")"
					       + " values "
					       + "('"  + Integer.parseInt(startLocationId)
					       + "','" + Integer.parseInt(endLocationId)
					       + "')";
			
			System.out.println(query);
			roleQuery = conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			result = roleQuery.executeUpdate();
			RouteRelationDao routeRelationDao = new RouteRelationDao();
			if(result>0){
				genratedTuple = roleQuery.getGeneratedKeys();
				while (genratedTuple.next()) {
					routeId = genratedTuple.getString(1);
				}
				
			}
			
		}catch(Exception e) {
			System.out.println(e.toString());
		}
		
		return routeId;
	}
	
	
}
