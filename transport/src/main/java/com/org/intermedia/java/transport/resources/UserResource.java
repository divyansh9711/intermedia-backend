package com.org.intermedia.java.transport.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.org.intermedia.java.transport.payloads.LocationPayload;
import com.org.intermedia.java.transport.payloads.UserPayload;
import com.org.intermedia.java.transport.response.LocationResponse;
import com.org.intermedia.java.transport.response.UserResponse;
import com.org.intermedia.java.transport.services.LocationService;
import com.org.intermedia.java.transport.services.UserService;

@Path("/user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

	@POST
	@Path("getUser")
	public UserResponse getUser(UserPayload payload) throws Exception {
		return new UserService().getUser(payload);
	}
	
	@POST
	@Path("auth")
	public UserResponse authUser(UserPayload payload) throws Exception {
		return new UserService().getUser(payload);
	}
}
