package com.org.intermedia.java.transport.resources;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.org.intermedia.java.transport.payloads.RoutePayload;

import com.org.intermedia.java.transport.response.RouteResponse;
import com.org.intermedia.java.transport.services.RouteService;

@Path("/route")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RouteResource {

	@POST
	@Path("getRoute")
	public RouteResponse getRoute(RoutePayload payload) throws Exception{
		return new RouteService().getUser(payload);
	}
	@POST
	@Path("createRoute")
	public RouteResponse createRoute(RoutePayload payload) throws Exception {
		return new RouteService().createUser(payload);
	}
	@POST
	@Path("getSubRoute")
	public RouteResponse getSubRoute(RoutePayload payload) throws Exception {
		RouteService subRouteService = new RouteService();
		return subRouteService.getSubs(payload);
	}
}
