package com.org.intermedia.java.transport.payloads;
import java.util.ArrayList;

import com.org.intermedia.java.transport.models.Category;
import com.org.intermedia.java.transport.models.Location;
import com.org.intermedia.java.transport.models.Route;
import com.org.intermedia.java.transport.models.Timing;
import com.org.intermedia.java.transport.models.User;
import com.org.intermedia.java.transport.models.Vehicle;

/**
 * @author Divyansh Singh
 *
 */
public class VehiclePayload extends Payload{

	private Vehicle vehicle;
	private String vehicleId;
	private String vehilceRegNum;
	private Timing timing;
	private String startLocation;
	private String endLocation;
	
	public String getEndLocation() {
		return endLocation;
	}
	public void setEndLocation(String endLocation) {
		this.endLocation = endLocation;
	}
	public String getStartLocation() {
		return startLocation;
	}
	public void setStartLocation(String startLocation) {
		this.startLocation = startLocation;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getVehilceRegNum() {
		return vehilceRegNum;
	}
	public void setVehilceRegNum(String vehilceRegNum) {
		this.vehilceRegNum = vehilceRegNum;
	}
	public Timing getTiming() {
		return timing;
	}
	public void setTiming(Timing timing) {
		this.timing = timing;
	}
}