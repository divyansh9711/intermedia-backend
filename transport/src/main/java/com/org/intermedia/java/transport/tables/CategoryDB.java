package com.org.intermedia.java.transport.tables;

public class CategoryDB {
	public static String categoryId = "categoryId";
	public static String categoryName = "categoryName";
	public static String categoryInfo = "categoryInfo";
	public static String parent = "parent";
	public static String getCategoryId() {
		return categoryId;
	}
	public static void setCategoryId(String categoryId) {
		CategoryDB.categoryId = categoryId;
	}
	public static String getCategoryName() {
		return categoryName;
	}
	public static void setCategoryName(String categoryName) {
		CategoryDB.categoryName = categoryName;
	}
	public static String getCategoryInfo() {
		return categoryInfo;
	}
	public static void setCategoryInfo(String categoryInfo) {
		CategoryDB.categoryInfo = categoryInfo;
	}
	public static String getParent() {
		return parent;
	}
	public static void setParent(String parent) {
		CategoryDB.parent = parent;
	}
	
	
}
