package com.org.intermedia.java.transport.response;

import java.util.List;

import com.org.intermedia.java.transport.models.Location;

public class LocationResponse {
	private BaseResponse baseResponse;
	private Location location;
	private List<Location> locationList;
	
	public LocationResponse() {}
	
	public BaseResponse getBaseResponse() {
		return baseResponse;
	}

	public void setBaseResponse(BaseResponse baseResponse) {
		this.baseResponse = baseResponse;
	}

	public Location getLocation(Location payload) {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<Location> getLocationList() {
		return locationList;
	}

	public void setLocationList(List<Location> locationList) {
		this.locationList = locationList;
	}
}