package com.org.intermedia.java.transport.dao;

import java.sql.*;

import com.org.intermedia.java.transport.models.Category;
import com.org.intermedia.java.transport.models.User;
import com.org.intermedia.java.transport.payloads.UserPayload;
import com.org.intermedia.java.transport.response.BaseResponse;
import com.org.intermedia.java.transport.response.UserResponse;
import com.org.intermedia.java.transport.tables.UserDB;
import com.org.intermedia.java.transport.utils.Constants;

public class UserDao {

	public UserResponse createUser(UserPayload payload, Connection conn) {
		User user = payload.getUser();
		BaseResponse baseResponse = new BaseResponse();
		System.out.println(payload.getInfo());
		System.out.println("lol");
		PreparedStatement roleQuery = null;
		ResultSet genratedTuple;
		int result = -99;
		UserResponse userResponse = new UserResponse();
		userResponse.setBaseResponse(baseResponse);
		userResponse.setUser(user);
		boolean status = true;
		
		try {
			String query = "INSERT into"
					       + Constants.tableUser
					       + "(" + UserDB.getFirstName()
					       + "," + UserDB.getLastName()
					       + "," + UserDB.getMobile()
					       + "," + UserDB.getUserEmail()
					       + "," + UserDB.getUserCategory()
					       + "," + UserDB.getInfo()
					       + "," + UserDB.getUserKey()
					       + "," + UserDB.getGenderId() +")"
					       + " values "
					       + "('" + user.getFirstName()
					       + "','" + user.getLastName()
					       + "','" + user.getUserMobile()
					       + "','" + user.getUserEmail()
					       + "','" + user.getUserCategory().getCategoryId()
					       + "','" + user.getInfo()
					       + "','" + payload.getInfo()
					       + "','" + user.getGender().getCategoryId() +"')";
			System.out.println(query);
			roleQuery = conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			result = roleQuery.executeUpdate();
			if(result>0){
				genratedTuple = roleQuery.getGeneratedKeys();
				while (genratedTuple.next()) {
					user.setuId(genratedTuple.getString(1));
				}
			}
			
		}catch(Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		if(status) {
			baseResponse.setStatusCode(Constants.SU200);
			baseResponse.setMessage(Constants.USER_INSERTED_SUCESSFULLY);
			userResponse.setBaseResponse(baseResponse);
			userResponse.setUser(user);
		}else {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			userResponse.setBaseResponse(baseResponse);
		}
		return userResponse;
	}
	
	/*
	 * public UserResponse getUser(UserPayload payload,Connection conn) {
	 * BaseResponse baseResponse = new BaseResponse(); String userId =
	 * payload.getUserId(); UserResponse userResponse = new UserResponse();
	 * PreparedStatement roleQuery = null; ResultSet result = null; User user = new
	 * User(); boolean status = true; try { String query = "select * from " +
	 * Constants.tableUser + " where " + UserDB.getUserId() + " = " + userId;
	 * System.out.println(query); roleQuery = conn.prepareStatement(query); result =
	 * roleQuery.executeQuery();
	 * 
	 * while(result.next()) { user.setuId(userId);
	 * user.setFirstName(result.getString(UserDB.getFirstName()));
	 * user.setLastName(result.getString(UserDB.getLastName())); Category category =
	 * new Category();
	 * category.setCategoryId(String.valueOf(result.getInt(UserDB.getGenderId())));
	 * user.setGender(category);
	 * category.setCategoryId(String.valueOf(result.getInt(UserDB.getUserCategory())
	 * )); user.setUserCategory(category);
	 * user.setUserMobile(result.getString(UserDB.getMobile()));
	 * user.setUserEmail(result.getString(UserDB.getUserEmail()));
	 * user.setInfo(result.getString(UserDB.getInfo())); }
	 * userResponse.setUser(user); }catch(SQLException e) {
	 * System.out.println(e.toString()); status = false; }catch(Exception e) {
	 * System.out.println(e.toString()); status = false; } if(status) {
	 * baseResponse.setStatusCode(Constants.SU200);
	 * baseResponse.setMessage(Constants.LOCATION_INSERTED_SUCESSFULLY);
	 * userResponse.setBaseResponse(baseResponse); userResponse.setUser(user); }else
	 * { baseResponse.setStatusCode(Constants.SU500);
	 * baseResponse.setMessage(Constants.SERVER_ERROR);
	 * userResponse.setBaseResponse(baseResponse); } return userResponse;
	 * 
	 * }
	 */
	
	
	public UserResponse authUser(UserPayload payload,Connection conn) {
		BaseResponse baseResponse = new BaseResponse();
		String userId = payload.getInfo();
		UserResponse userResponse = new UserResponse();
		PreparedStatement roleQuery = null;
		ResultSet result = null;
		boolean resSet = true;
		User user = new User();
		boolean status = true;
		try {
			String query = "select * from " + Constants.tableUser + " where " + UserDB.getUserKey() + " = '" + userId + "'";
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			
			while(result.next()) {
				resSet = false;
				user.setuId(userId);
				user.setFirstName(result.getString(UserDB.getFirstName()));
				user.setLastName(result.getString(UserDB.getLastName()));
				Category category = new Category();
				category.setCategoryId(String.valueOf(result.getInt(UserDB.getGenderId())));
				user.setGender(category);
				category.setCategoryId(String.valueOf(result.getInt(UserDB.getUserCategory())));
				user.setUserCategory(category);
				user.setUserMobile(result.getString(UserDB.getMobile()));
				user.setUserEmail(result.getString(UserDB.getUserEmail()));
				user.setInfo(result.getString(UserDB.getInfo()));
			}
			userResponse.setUser(user);
		}catch(SQLException e) {
			System.out.println(e.toString());
			status = false;
		}catch(Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		if(status) {
			baseResponse.setStatusCode(Constants.SU200);
			baseResponse.setMessage(Constants.LOCATION_INSERTED_SUCESSFULLY);
			userResponse.setBaseResponse(baseResponse);
			userResponse.setUser(user);
		}else {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			userResponse.setBaseResponse(baseResponse);
		}
		if(resSet) {
			return createUser(payload,conn);
		}
		return userResponse;
		
	}
	
	
	
	
	public User getUser(String userId,Connection conn) {

		PreparedStatement roleQuery = null;
		ResultSet result = null;
		User user = new User();
		try {
			String query = "select * from " + Constants.tableUser + " where " + UserDB.getUserId() + " = " + userId ;
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			
			while(result.next()) {
				user.setuId(userId);
				user.setFirstName(result.getString(UserDB.getFirstName()));
				user.setLastName(result.getString(UserDB.getLastName()));
				Category category = new Category();
				category.setCategoryId(String.valueOf(result.getInt(UserDB.getGenderId())));
				user.setGender(category);
				category.setCategoryId(String.valueOf(result.getInt(UserDB.getUserCategory())));
				user.setUserCategory(category);
				user.setUserMobile(result.getString(UserDB.getMobile()));
				user.setUserEmail(result.getString(UserDB.getUserEmail()));
				user.setInfo(result.getString(UserDB.getInfo()));
				return user;
			}
		}catch(SQLException e) {
			System.out.println(e.toString());
			
		}catch(Exception e) {
			System.out.println(e.toString());
			
		}
		return user;
		
	}
	
}
