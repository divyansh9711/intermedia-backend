package com.org.intermedia.java.transport.tables;

public class LocationDB {

	private static String tableName = "location";
	
	private static String locationId = "locationId";
	private static String title = "title";
	private static String info = "info";
	private static String latitude = "latitude";
	private static String longitude = "longitude";
	private static String locality = "locality";
	private static String zipCode = "zipCode";
	private static String city = "city";
	private static String state = "state";
	private static String country = "country";
	
	public static String getLocationId() {
		return locationId;
	}
	public static void setLocationId(String locationId) {
		LocationDB.locationId = locationId;
	}
	public static String getTableName() {
		return tableName;
	}
	public static void setTableName(String tableName) {
		LocationDB.tableName = tableName;
	}
	public static String getTitle() {
		return title;
	}
	public static void setTitle(String title) {
		LocationDB.title = title;
	}
	public static String getInfo() {
		return info;
	}
	public static void setInfo(String info) {
		LocationDB.info = info;
	}
	public static String getLatitude() {
		return latitude;
	}
	public static void setLatitude(String latitude) {
		LocationDB.latitude = latitude;
	}
	public static String getLongitude() {
		return longitude;
	}
	public static void setLongitude(String longitude) {
		LocationDB.longitude = longitude;
	}
	public static String getLocality() {
		return locality;
	}
	public static void setLocality(String locality) {
		LocationDB.locality = locality;
	}
	public static String getZipCode() {
		return zipCode;
	}
	public static void setZipCode(String zipCode) {
		LocationDB.zipCode = zipCode;
	}
	public static String getCity() {
		return city;
	}
	public static void setCity(String city) {
		LocationDB.city = city;
	}
	public static String getState() {
		return state;
	}
	public static void setState(String state) {
		LocationDB.state = state;
	}
	public static String getCountry() {
		return country;
	}
	public static void setCountry(String country) {
		LocationDB.country = country;
	}
	
	
}