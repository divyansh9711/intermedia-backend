package com.org.intermedia.java.transport.payloads;

import com.org.intermedia.java.transport.models.Location;

public class LocationPayload extends Payload {
	
	private Location location;
	private String locationId;
	private String zipcode;
	
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
}
