package com.org.intermedia.java.transport.models;

/**
 * @author Divyansh Singh
 *
 */
public class User extends Profile {

	private String firstName;
	private String lastName;
	private String userName;
	private String userMobile;
	
	private String userEmail;
	private String userImage;
	private Category gender;
	private Category userCategory;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserMobile() {
		return userMobile;
	}
	public void setUserMobile(String userPh) {
		this.userMobile = userPh;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserImage() {
		return userImage;
	}
	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}
	public Category getGender() {
		return gender;
	}
	public void setGender(Category gender) {
		this.gender = gender;
	}
	public Category getUserCategory() {
		return userCategory;
	}
	public void setUserCategory(Category userCategory) {
		this.userCategory = userCategory;
	}
	
	
}