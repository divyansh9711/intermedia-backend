package com.org.intermedia.java.transport.utils;

public class Constants {

	
	/*  ---------------Table names (MySql)-----------------  */
	public static String tableBus = " bus ";
	public static String tableLocation = " location ";
	public static String tableRoute = " route ";
	public static String tableUser = " user ";
	public static String tableVehicle = " bus ";
	public static String tableRouteRelation = " route_route ";
	public static String tableCategory = " category ";

	/*  ---------Response codes and messages---------------  */
	public static String SU200 = "200";	// Success
	public static String SU500 = "500";	// Server error/exceptions
	public static String SU400 = "400";	// Item does not exist, ex- email not registered
	public static String SU410 = "410";	// Item already exist,  ex- email, ad already registered
	public static String SU401 = "401"; 	// Email - not active
	public static String SU402 = "402"; 	// Password do not match
	
	public static String LOCATION_RETRIVED_SUCESSFULLY = "Location retrived succefully";
	public static String LOCATION_INSERTED_SUCESSFULLY = "Location inserted succefully";
	public static String USER_INSERTED_SUCESSFULLY = "Succefull";
	public static String USER_RETRIVAL_SUCESSFULLY = "Invalid Payload";



	public static String SERVER_ERROR = "Server error";
}