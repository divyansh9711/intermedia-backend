package com.org.intermedia.java.transport.payloads;

import com.org.intermedia.java.transport.models.User;
/**
 * @author Divyansh Singh
 *
 */
public class UserPayload extends Payload {

	private User user;
	private String userId;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
}