package com.org.intermedia.java.transport.services;

import java.sql.Connection;

import com.org.intermedia.java.transport.dao.UserDao;
import com.org.intermedia.java.transport.dao.VehicleDao;
import com.org.intermedia.java.transport.payloads.UserPayload;
import com.org.intermedia.java.transport.payloads.VehiclePayload;
import com.org.intermedia.java.transport.response.BaseResponse;
import com.org.intermedia.java.transport.response.UserResponse;
import com.org.intermedia.java.transport.response.VehicleResponse;
import com.org.intermedia.java.transport.utils.Constants;

public class VehicleService {

	public VehicleResponse createVehicle(VehiclePayload payload) throws Exception{
		
		Connection connection = getConnection();
		VehicleDao vehicleDao = new VehicleDao();
		return vehicleDao.createVehicle(payload, connection);
	}
	private Connection getConnection() throws Exception {
		DbConnection dbConnection=new DbConnection();
		Connection connection = dbConnection.getConnection();
		return connection;
	}
	public VehicleResponse getVehicle(VehiclePayload payload) throws Exception{
		
		if(checkPayload(payload)) {
			BaseResponse baseResponse = new BaseResponse();
			VehicleResponse vehicleResponse = new VehicleResponse();
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			vehicleResponse.setBaseResponse(baseResponse);
			return vehicleResponse;
		}
		Connection connection = getConnection();
		VehicleDao vehicleDao = new VehicleDao();
		return vehicleDao.getVehicle(payload, connection);
	}
	public boolean checkPayload(VehiclePayload payload) {
		if(payload == null || String.valueOf(payload.getStart()) != null && String.valueOf(payload.getLimit()) != null ) {
			return false;
		}else if(payload.getVehicle() == null && payload.getTiming() == null && payload.getTiming().getWeekDay() == null) {
			return true;
		}
		return false;
	}
}
