package com.org.intermedia.java.transport.tables;

public class VehicleDB {
	private static String tableName = "bus";
	private static String busId = "busId";
	private static String plateId = "plateId";
	private static String routeId = "routeId";
	private static String vehicleName = "vehicleName";
	private static String vehicleCategory = "vehicleCategory";
	private static String weekDay = "day";
	private static String ownerId = "creatorId";
	private static String startLocationId = "startLocationId";
	private static String endLocationId = "endLocationId";
	private static String driverId = "driverId";
	private static String startTime = "startTime";
	private static String endTime = "endTime";
	
	
	
	public static String getStartLocationId() {
		return startLocationId;
	}
	public static void setStartLocationId(String startLocationId) {
		VehicleDB.startLocationId = startLocationId;
	}
	public static String getEndLocationId() {
		return endLocationId;
	}
	public static void setEndLocationId(String endLocationId) {
		VehicleDB.endLocationId = endLocationId;
	}
	public static String getOwnerId() {
		return ownerId;
	}
	public static void setOwnerId(String ownerId) {
		VehicleDB.ownerId = ownerId;
	}
	public static String getTableName() {
		return tableName;
	}
	public static void setTableName(String tableName) {
		VehicleDB.tableName = tableName;
	}
	public static String getBusId() {
		return busId;
	}
	public static void setBusId(String busId) {
		VehicleDB.busId = busId;
	}
	public static String getPlateId() {
		return plateId;
	}
	public static void setPlateId(String plateId) {
		VehicleDB.plateId = plateId;
	}
	public static String getRouteId() {
		return routeId;
	}
	public static void setRouteId(String routeId) {
		VehicleDB.routeId = routeId;
	}
	public static String getVehicleName() {
		return vehicleName;
	}
	public static void setVehicleName(String vehicleName) {
		VehicleDB.vehicleName = vehicleName;
	}
	public static String getVehicleCategory() {
		return vehicleCategory;
	}
	public static void setVehicleCategory(String vehicleCategory) {
		VehicleDB.vehicleCategory = vehicleCategory;
	}
	public static String getWeekDay() {
		return weekDay;
	}
	public static void setWeekDay(String day) {
		VehicleDB.weekDay = day;
	}
	public static String getDriverId() {
		return driverId;
	}
	public static void setDriverId(String driverId) {
		VehicleDB.driverId = driverId;
	}
	public static String getStartTime() {
		return startTime;
	}
	public static void setStartTime(String startTime) {
		VehicleDB.startTime = startTime;
	}
	public static String getEndTime() {
		return endTime;
	}
	public static void setEndTime(String endTime) {
		VehicleDB.endTime = endTime;
	}
	
	
	
}