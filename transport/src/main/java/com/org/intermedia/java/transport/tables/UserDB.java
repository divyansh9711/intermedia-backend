package com.org.intermedia.java.transport.tables;

public class UserDB {

	private static String tableName = "user";
	private static String userId = "userId";
	private static String firstName = "firstName";
	private static String lastName = "lastName";
	private static String mobile = "mobile";
	private static String userEmail = "userEmail";
	private static String genderId = "genderId";
	private static String userCategory = "userCategory";
	private static String info = "info";
	private static String userKey = "userKey";
	
	
	public static String getUserKey() {
		return userKey;
	}
	public static void setUserKey(String userKey) {
		UserDB.userKey = userKey;
	}
	public static String getTableName() {
		return tableName;
	}
	public static void setTableName(String tableName) {
		UserDB.tableName = tableName;
	}
	public static String getUserId() {
		return userId;
	}
	public static void setUserId(String userId) {
		UserDB.userId = userId;
	}
	public static String getFirstName() {
		return firstName;
	}
	public static void setFirstName(String firstName) {
		UserDB.firstName = firstName;
	}
	public static String getLastName() {
		return lastName;
	}
	public static void setLastName(String lastName) {
		UserDB.lastName = lastName;
	}
	public static String getMobile() {
		return mobile;
	}
	public static void setMobile(String mobile) {
		UserDB.mobile = mobile;
	}
	public static String getUserEmail() {
		return userEmail;
	}
	public static void setUserEmail(String userEmail) {
		UserDB.userEmail = userEmail;
	}
	public static String getGenderId() {
		return genderId;
	}
	public static void setGenderId(String genderId) {
		UserDB.genderId = genderId;
	}
	public static String getUserCategory() {
		return userCategory;
	}
	public static void setUserCategory(String userCategory) {
		UserDB.userCategory = userCategory;
	}
	public static String getInfo() {
		return info;
	}
	public static void setInfo(String info) {
		UserDB.info = info;
	}
	
	
}
