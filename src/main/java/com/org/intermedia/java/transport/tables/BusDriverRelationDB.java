package com.org.intermedia.java.transport.tables;

public class BusDriverRelationDB {
	public static String busId = "busId";
	public static String driverId = "driverId";
	public static String id = "id";
	public static String getBusId() {
		return busId;
	}
	public static void setBusId(String busId) {
		BusDriverRelationDB.busId = busId;
	}
	public static String getDriverId() {
		return driverId;
	}
	public static void setDriverId(String driverId) {
		BusDriverRelationDB.driverId = driverId;
	}
	public static String getId() {
		return id;
	}
	public static void setId(String id) {
		BusDriverRelationDB.id = id;
	}
	
	
}
