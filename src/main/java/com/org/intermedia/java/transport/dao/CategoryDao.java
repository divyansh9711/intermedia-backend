package com.org.intermedia.java.transport.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.org.intermedia.java.transport.models.Category;
import com.org.intermedia.java.transport.tables.CategoryDB;
import com.org.intermedia.java.transport.utils.Constants;

public class CategoryDao {

	public Category getCategory(String categoryId,Connection conn) {
		String result = "-99";
		Category category = new Category();
		PreparedStatement roleQuery = null;
		ResultSet resultSet = null;
		category.setCategoryId(result);
		category.setCategoryTitle(result);
		category.setCategoryInfo(result);
		try {
			String query = "select * from" + Constants.tableCategory + " where " + CategoryDB.getCategoryId() + " = " + categoryId;
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			resultSet = roleQuery.executeQuery();
			while(resultSet.next()) {
				category.setCategoryId(resultSet.getString(CategoryDB.getCategoryId()));
				category.setCategoryTitle(resultSet.getString(CategoryDB.getCategoryName()));
				category.setCategoryInfo(resultSet.getString(CategoryDB.getCategoryInfo()));
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return category;
	}
}
