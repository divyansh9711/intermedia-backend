package com.org.intermedia.java.transport.models;

public class Timing {

	private String startDate;
	private String endDate;
	private String startTime;
	private String endTime;
	private String weekDay;
	
	
	
	public String getWeekDay() {
		return weekDay;
	}
	public void setWeekDay(String weekDay) {
		this.weekDay = weekDay;
	}
	public Timing () {super();}
	public Timing(String startTime,String endTime,String weekDay) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.weekDay = weekDay;
	}
	
	public Timing(String startTime, String endTime) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	
}
