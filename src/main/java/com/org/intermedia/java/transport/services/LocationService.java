package com.org.intermedia.java.transport.services;
import java.sql.Connection;

import com.org.intermedia.java.transport.dao.LocationsDao;
import com.org.intermedia.java.transport.models.Location;
import com.org.intermedia.java.transport.payloads.LocationPayload;
import com.org.intermedia.java.transport.response.BaseResponse;
import com.org.intermedia.java.transport.response.LocationResponse;
import com.org.intermedia.java.transport.utils.Constants;

public class LocationService {
	
	public LocationResponse getLocation(LocationPayload payload) throws Exception {
		Connection connection = getConnection();
		LocationsDao locationDao = new LocationsDao();
		return locationDao.getLocation(payload,connection);
	}
	
	
	public LocationResponse createLocation(LocationPayload payload) throws Exception{
		if(checkPayload(payload)) {
			BaseResponse baseResponse = new BaseResponse();
			LocationResponse locationResponse = new LocationResponse();
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			locationResponse.setBaseResponse(baseResponse);
			return locationResponse;
		}
		Connection connection = getConnection();
		LocationsDao locationDao = new LocationsDao();
		return locationDao.createLocation(payload, connection);
	}
	
	
	private Connection getConnection() throws Exception {
		DbConnection dbConnection=new DbConnection();
		Connection connection = dbConnection.getConnection();
		return connection;
	}
	
	
	private boolean checkPayload(LocationPayload lPayload) {
		Location payload = lPayload.getLocation();
		if(payload == null || 
		   payload.getTitle() == null || 
		   payload.getCity() == null || 
		   payload.getZipCode() == null || 
		   payload.getState() == null || 
		   payload.getCountry() == null) {
			return true;
		}else if(payload.getTitle().equals("") ||
				 payload.getCity().equals("") ||
				 payload.getZipCode().equals("") ||
				 payload.getState().equals("") ||
				 payload.getCountry().equals("")) {
			return true;
		}
		return false;
	}
}
