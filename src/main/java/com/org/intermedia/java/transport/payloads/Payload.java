package com.org.intermedia.java.transport.payloads;

public class Payload {

	private String APIKey;		 
	private String info;	
	private String id;
	private String searchKey;	 
	private String sortBy;        
	private boolean sortOrder;// default:0, desc-0, asc-1
	private int start;         	 // default=0
	private int limit;            
	private UserKey userKey;
	public Payload() {}

	public Payload(String aPIKey, String campusId, String searchKey, String sortBy, boolean sortOrder, int start,
			int limit) {
		super();
		APIKey = aPIKey;
		this.searchKey = searchKey;
		this.sortBy = sortBy;
		this.sortOrder = sortOrder;
		this.start = start;
		this.limit = limit;
	}
	

	public String getInfo() {
		return info;
	}
	

	
	public void setUserKey(UserKey userKey) {this.userKey = userKey;}
	public UserKey getUserKey() {return userKey;}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getAPIKey() {
		return APIKey;
	}
	
	public void setAPIKey(String aPIKey) {
		APIKey = aPIKey;
	}
	

	
	public String getSearchKey() {
		return searchKey;
	}
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	
	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	
	public boolean isSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(boolean sortOrder) {
		this.sortOrder = sortOrder;
	}

	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



	
}
