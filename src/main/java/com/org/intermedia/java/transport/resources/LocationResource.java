package com.org.intermedia.java.transport.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.org.intermedia.java.transport.payloads.LocationPayload;
import com.org.intermedia.java.transport.response.LocationResponse;
import com.org.intermedia.java.transport.services.LocationService;

@Path("/location")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LocationResource {
	
	@POST
	@Path("getLocation")
	public LocationResponse getLocation(LocationPayload payload) throws Exception{
		LocationService locationService = new LocationService();
		return locationService.getLocation(payload);
	}
	@POST
	@Path("createLocation")
	public LocationResponse createLocation(LocationPayload payload) throws Exception{
		LocationService locationService = new LocationService();
		return locationService.createLocation(payload);
	}
	
}
