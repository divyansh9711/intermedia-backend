package com.org.intermedia.java.transport.models;

/**
 * @author Divyansh Singh
 *
 */
public class Profile {

	private String uId;
	private String title;
	private String info;
	private Category category;
	private Media media;
	private Analytics analytics;
	//private Status status;
	private CreatorInfo creator;
	
	public Profile() {}
	
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Media getMedia() {
		return media;
	}
	public void setMedia(Media media) {
		this.media = media;
	}
	public Analytics getAnalytics() {
		return analytics;
	}
	public void setAnalytics(Analytics analytics) {
		this.analytics = analytics;
	}
	
	public CreatorInfo getCreator() {
		return creator;
	}
	public void setCreator(CreatorInfo creator) {
		this.creator = creator;
	}
	
	
}