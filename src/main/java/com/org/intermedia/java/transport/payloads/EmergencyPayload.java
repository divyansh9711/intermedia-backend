package com.org.intermedia.java.transport.payloads;

public class EmergencyPayload extends Payload {

	public String vehicleId;
	public String message;
	public String status;
	public String alertStatus;
	
	
	
	
	public String getAlertStatus() {
		return alertStatus;
	}

	void setAlertStatus(String alertStatus) {
		this.alertStatus = alertStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public EmergencyPayload() {}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
