package com.org.intermedia.java.transport.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.org.intermedia.java.transport.models.Category;
import com.org.intermedia.java.transport.models.Route;
import com.org.intermedia.java.transport.payloads.RoutePayload;
import com.org.intermedia.java.transport.response.BaseResponse;
import com.org.intermedia.java.transport.response.RouteResponse;
import com.org.intermedia.java.transport.tables.RouteDB;
import com.org.intermedia.java.transport.tables.RouteRelationDB;
import com.org.intermedia.java.transport.utils.Constants;

public class RouteRelationDao {
	public boolean createRoute(Route route, Connection conn) {

		List<Route> subRoutes =  route.getSubRoute();
		PreparedStatement roleQuery = null;
		int result = -99;
	
		boolean status = true;
		
		try {
			for(Route r : subRoutes) {
				String subQuery = "Insert into"
								  + Constants.tableRouteRelation
								  + "(" + RouteRelationDB.getParentRouteId()
								  + "," + RouteRelationDB.getSubRouteId() + ")"
								  + " values "
								  + "(" + route.getuId()
								  + "," + r.getuId() + ")";
				System.out.println(subQuery);
				roleQuery = conn.prepareStatement(subQuery);
				result = roleQuery.executeUpdate();
				if(result > 0) {status = true;}
			}
		}catch(Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		return status;
	}
	
	public ArrayList<Route> getSubRoutes(String routeId, Connection conn){
		
		PreparedStatement roleQuery = null;
		ResultSet result = null;
		Route route = new Route();
		String subRouteId = "";
		ArrayList<Route> subRoutes = new ArrayList<Route>();
		try {
			String query = "select * from " + Constants.tableRouteRelation + " where " + RouteRelationDB.getParentRouteId() + " = " + routeId;
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			
			while(result.next()) {
				subRouteId = String.valueOf(result.getInt(RouteRelationDB.getSubRouteId()));
				route = new RouteDao().getRoute(subRouteId, conn);
				subRoutes.add(route);
				System.out.println("\n new route added = " + route.getTitle());
			}
		}catch(SQLException e) {
			System.out.println(e.toString());
			
		}catch(Exception e) {
			System.out.println(e.toString());
			
		}
		
		return subRoutes;
	}
}
