package com.org.intermedia.java.transport.services;

import java.sql.Connection;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

import com.org.intermedia.java.transport.dao.RouteDao;
import com.org.intermedia.java.transport.dao.RouteRelationDao;
import com.org.intermedia.java.transport.models.Route;
import com.org.intermedia.java.transport.models.Vehicle;
import com.org.intermedia.java.transport.payloads.*;
import com.org.intermedia.java.transport.response.BaseResponse;
import com.org.intermedia.java.transport.response.RouteResponse;
import com.org.intermedia.java.transport.utils.Constants;

public class RouteService {
	public RouteResponse createUser(RoutePayload payload) throws Exception{
		if(checkPayload(payload)) {
			BaseResponse baseResponse = new BaseResponse();
			RouteResponse routeResponse = new RouteResponse();
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			routeResponse.setBaseResponse(baseResponse);
			return routeResponse;
		}
		Connection connection = getConnection();
		RouteDao routeDao = new RouteDao();
		return routeDao.createRoute(payload, connection);
	}
	private Connection getConnection() throws Exception {
		DbConnection dbConnection=new DbConnection();
		Connection connection = dbConnection.getConnection();
		return connection;
	}
	public RouteResponse getUser(RoutePayload payload) throws Exception{
		Connection connection = getConnection();
		RouteDao routeDao = new RouteDao();
		return routeDao.getRoute(payload, connection);
	}
	public boolean checkPayload(RoutePayload payload) {
		if(payload == null || payload.getRoute() == null) {
			return true;
		}
		return false;
	}
	public RouteResponse getSubs(RoutePayload payload) throws Exception {
		Connection conn = getConnection();
		RouteResponse routeResponse = new RouteResponse();
		BaseResponse baseResponse = new BaseResponse();

		RouteRelationDao routeRelationDao = new RouteRelationDao();
		if(payload.getRouteId() != null) {
			ArrayList<Route> sub = new ArrayList<>();
			sub.addAll(routeRelationDao.getSubRoutes(payload.getRouteId(), conn));
			routeResponse.setRouteList(sub);
			baseResponse.setStatusCode(Constants.SU200);
			baseResponse.setMessage(Constants.LOCATION_RETRIVED_SUCESSFULLY);
			routeResponse.setBaseResponse(baseResponse);
			
		}else {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			routeResponse.setBaseResponse(baseResponse);
		}
		return routeResponse;
	}
}
