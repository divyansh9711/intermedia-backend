package com.org.intermedia.java.transport.models;

import java.util.ArrayList;
import java.util.List;

public class Route extends Profile {

	
	private Location startLocation;
	private Location endLocation;
	private List<Route> subRoutes;
	private Timing interval;
	private ArrayList<Timing> timing;
	private String fare;
	
	public List<Route> getSubRoute() {
		return subRoutes;
	}
	public Location getStartLocation() {
		return startLocation;
	}
	public void setStartLocation(Location startLocation) {
		this.startLocation = startLocation;
	}
	public Location getEndLocation() {
		return endLocation;
	}
	public void setEndLocation(Location endLocation) {
		this.endLocation = endLocation;
	}
	
	public void setSubRoutes(List<Route> s) {
		this.subRoutes = s;
	}
	public Timing getInterval() {
		return interval;
	}
	public void setInterval(Timing interval) {
		this.interval = interval;
	}
	public ArrayList<Timing> getTiming() {
		return timing;
	}
	public void setTiming(ArrayList<Timing> timing) {
		this.timing = timing;
	}
	public String getFare() {
		return fare;
	}
	public void setFare(String fare) {
		this.fare = fare;
	}
	
	
	
	
	
}