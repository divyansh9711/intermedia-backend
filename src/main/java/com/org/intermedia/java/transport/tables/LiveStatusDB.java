package com.org.intermedia.java.transport.tables;

public class LiveStatusDB {
	public static String tableName = "live_bus";
	public static String busId = "busId";
	public static String latitude = "latitude";
	public static String longitude = "longitude";
	public static String Status = "status";
	public static String driverId = "driverId";
	public static String alertStatus = "alertStatus";
	public static String message = "message";
	
	
	
	public static String getAlertStatus() {
		return alertStatus;
	}
	public static void setAlertStatus(String alertStatus) {
		LiveStatusDB.alertStatus = alertStatus;
	}
	public static String getMessage() {
		return message;
	}
	public static void setMessage(String message) {
		LiveStatusDB.message = message;
	}
	public static String getDriverId() {
		return driverId;
	}
	public static void setDriverId(String driverId) {
		LiveStatusDB.driverId = driverId;
	}
	public static String getTableName() {
		return tableName;
	}
	public static void setTableName(String tableName) {
		LiveStatusDB.tableName = tableName;
	}
	public static String getBusId() {
		return busId;
	}
	public static void setBusId(String busId) {
		LiveStatusDB.busId = busId;
	}
	public static String getLatitude() {
		return latitude;
	}
	public static void setLatitude(String latitude) {
		LiveStatusDB.latitude = latitude;
	}
	public static String getLongitude() {
		return longitude;
	}
	public static void setLongitude(String longitude) {
		LiveStatusDB.longitude = longitude;
	}
	public static String getStatus() {
		return Status;
	}
	public static void setStatus(String status) {
		Status = status;
	}
	
	
}
