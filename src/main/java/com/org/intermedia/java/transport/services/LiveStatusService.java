package com.org.intermedia.java.transport.services;

import java.sql.Connection;

import com.org.intermedia.java.transport.dao.LiveStatusDao;
import com.org.intermedia.java.transport.payloads.EmergencyPayload;
import com.org.intermedia.java.transport.response.EmergencyResponse;
import com.org.intermedia.java.transport.utils.OkHTTPRequest;

public class LiveStatusService {

	public EmergencyResponse setEmergencyStatus(EmergencyPayload payload) throws Exception {
		Connection conn = getConnection();
		System.out.println("reached gere");
		EmergencyResponse e =  new LiveStatusDao().setEmergencyStatus(payload,conn);
		String o = new OkHTTPRequest().sendEmergencyPost(e);
		return e;
	}
	
	public EmergencyResponse setAlert(EmergencyPayload payload) throws Exception {
		Connection conn = getConnection();
		return new LiveStatusDao().setAlert(payload,conn);
	}
	public EmergencyResponse getEmergency() throws Exception{
		Connection conn = getConnection();
		return new LiveStatusDao().getEmergency(conn);
	}
	
	private Connection getConnection() throws Exception {
		DbConnection dbConnection=new DbConnection();
		Connection connection = dbConnection.getConnection();
		return connection;
	}
	public EmergencyResponse getAlerted() throws Exception{
		Connection conn = getConnection();
		return new LiveStatusDao().getAlerted(conn);
	}
	public EmergencyResponse resolve(EmergencyPayload payload) throws Exception{
		Connection conn = getConnection();
		return new LiveStatusDao().resolve(payload,conn);
	}
}
