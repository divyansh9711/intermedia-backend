package com.org.intermedia.java.transport.response;

import java.util.ArrayList;

import com.org.intermedia.java.transport.models.Vehicle;

public class VehicleResponse {

	private BaseResponse baseResponse;
	private Vehicle vehicle;
	private ArrayList<Vehicle> vehicleList;
	
	public BaseResponse getBaseResponse() {
		return baseResponse;
	}
	public void setBaseResponse(BaseResponse baseResponse) {
		this.baseResponse = baseResponse;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public ArrayList<Vehicle> getVehicleList() {
		return vehicleList;
	}
	public void setVehicleList(ArrayList<Vehicle> vehicleList) {
		this.vehicleList = vehicleList;
	}
}