package com.org.intermedia.java.transport.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.org.intermedia.java.transport.models.Category;
import com.org.intermedia.java.transport.models.Location;
import com.org.intermedia.java.transport.models.Route;
import com.org.intermedia.java.transport.models.Timing;
import com.org.intermedia.java.transport.models.User;
import com.org.intermedia.java.transport.models.Vehicle;
import com.org.intermedia.java.transport.models.Vehicle;
import com.org.intermedia.java.transport.payloads.InitialiseVehiclePayload;
import com.org.intermedia.java.transport.payloads.LocationPayload;
import com.org.intermedia.java.transport.payloads.VehiclePayload;
import com.org.intermedia.java.transport.payloads.VehiclePayload;
import com.org.intermedia.java.transport.response.BaseResponse;
import com.org.intermedia.java.transport.response.VehicleResponse;
import com.org.intermedia.java.transport.response.VehicleResponse;
import com.org.intermedia.java.transport.tables.VehicleDB;
import com.org.intermedia.java.transport.tables.VehicleDB;
import com.org.intermedia.java.transport.utils.Constants;

public class VehicleDao {

	public VehicleResponse getVehicle(VehiclePayload payload,Connection conn) {
		
		BaseResponse baseResponse = new BaseResponse();
		VehicleResponse vehicleResponse = new VehicleResponse();
		PreparedStatement roleQuery = null;
		ResultSet result = null;
		
		boolean status = true;
		ArrayList<Vehicle> vehicleList = new ArrayList<>();
		
		
		
		try {
			String query = makeQuery(payload,conn);
			
			
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			int driverId = -99;
			int ownerId = -99;
			while(result.next()) {
				Vehicle vehicle = new Vehicle();
				vehicle.setuId(result.getString(VehicleDB.getBusId()));
				vehicle.setVehicleName(result.getString(VehicleDB.getVehicleName()));
				//System.out.println(result.getString(VehicleDB.getVehicleName()));
				
				Category category = new Category();
				category.setCategoryId(String.valueOf(result.getInt(VehicleDB.getVehicleCategory())));
				vehicle.setCategory(category);
				
				Timing timings = new Timing(String.valueOf(result.getTime(VehicleDB.getStartTime())),
											String.valueOf(result.getTime(VehicleDB.getEndTime())),
											"-1");
				timings.setWeekDay(result.getString("day"));
				System.out.println(String.valueOf(result.getString(VehicleDB.getWeekDay())));
				vehicle.setTiming(timings);
				
				User user = new UserDao().getUser(result.getString(VehicleDB.getDriverId()), conn);
				vehicle.setDriver(user);
				Location location = new LocationsDao().getLocation(result.getString(VehicleDB.getStartLocationId()), conn);
				vehicle.setStartLocation(location);
				
				location = new LocationsDao().getLocation(result.getString(VehicleDB.getEndLocationId()), conn);
				vehicle.setEndLocation(location);
				
				Route route = new RouteDao().getRoute(result.getString(VehicleDB.getRouteId()), conn);
				vehicle.setRoute(route);
				
				vehicleList.add(vehicle);
				System.out.println(vehicle.getTitle());
			}
			
		}catch(Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		if(status) {
			baseResponse.setStatusCode(Constants.SU200); 
			baseResponse.setMessage(Constants.LOCATION_RETRIVED_SUCESSFULLY);
			vehicleResponse.setBaseResponse(baseResponse);
			vehicleResponse.setVehicleList(vehicleList);
		}
		else {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			vehicleResponse.setBaseResponse(baseResponse);
		}
		return vehicleResponse;
	}
	
public VehicleResponse get(VehiclePayload payload,Connection conn) {
		
		BaseResponse baseResponse = new BaseResponse();
		VehicleResponse vehicleResponse = new VehicleResponse();
		PreparedStatement roleQuery = null;
		ResultSet result = null;
		
		boolean status = true;
		ArrayList<Vehicle> vehicleList = new ArrayList<>();
		
		
		
		try {
			String query = "select * from " + Constants.tableVehicle + " where "
					+ VehicleDB.getBusId() + " = " + payload.getVehicleId();
			
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			int driverId = -99;
			int ownerId = -99;
			while(result.next()) {
				Vehicle vehicle = new Vehicle();
				vehicle.setuId(result.getString(VehicleDB.getBusId()));
				vehicle.setVehicleName(result.getString(VehicleDB.getVehicleName()));
				//System.out.println(result.getString(VehicleDB.getVehicleName()));
				
				Category category = new Category();
				category.setCategoryId(String.valueOf(result.getInt(VehicleDB.getVehicleCategory())));
				vehicle.setCategory(category);
				
				Timing timings = new Timing(String.valueOf(result.getTime(VehicleDB.getStartTime())),
											String.valueOf(result.getTime(VehicleDB.getEndTime())),
											"-1");
				timings.setWeekDay(result.getString("day"));
				System.out.println(String.valueOf(result.getString(VehicleDB.getWeekDay())));
				vehicle.setTiming(timings);
				
				User user = new UserDao().getUser(result.getString(VehicleDB.getDriverId()), conn);
				vehicle.setDriver(user);
				Location location = new LocationsDao().getLocation(result.getString(VehicleDB.getStartLocationId()), conn);
				vehicle.setStartLocation(location);
				
				location = new LocationsDao().getLocation(result.getString(VehicleDB.getEndLocationId()), conn);
				vehicle.setEndLocation(location);
				
				Route route = new RouteDao().getRoute(result.getString(VehicleDB.getRouteId()), conn);
				vehicle.setRoute(route);
				
				vehicleList.add(vehicle);
				System.out.println(vehicle.getTitle());
			}
			
		}catch(Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		if(status) {
			baseResponse.setStatusCode(Constants.SU200); 
			baseResponse.setMessage(Constants.LOCATION_RETRIVED_SUCESSFULLY);
			vehicleResponse.setBaseResponse(baseResponse);
			vehicleResponse.setVehicleList(vehicleList);
		}
		else {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			vehicleResponse.setBaseResponse(baseResponse);
		}
		return vehicleResponse;
	}

public Vehicle get(String id,Connection conn) {
	PreparedStatement roleQuery = null;
	ResultSet result = null;
	
	boolean status = true;
	ArrayList<Vehicle> vehicleList = new ArrayList<>();
	Vehicle vehicle = new Vehicle();

	
	
	try {
		String query = "select * from " + Constants.tableVehicle + " where "
				+ VehicleDB.getBusId() + " = " + id;
		
		System.out.println(query);
		roleQuery = conn.prepareStatement(query);
		result = roleQuery.executeQuery();
		int driverId = -99;
		int ownerId = -99;
		while(result.next()) {
			
			vehicle.setuId(result.getString(VehicleDB.getBusId()));
			vehicle.setVehicleName(result.getString(VehicleDB.getVehicleName()));
			//System.out.println(result.getString(VehicleDB.getVehicleName()));
			
			Category category = new Category();
			category.setCategoryId(String.valueOf(result.getInt(VehicleDB.getVehicleCategory())));
			vehicle.setCategory(category);
			
			Timing timings = new Timing(String.valueOf(result.getTime(VehicleDB.getStartTime())),
										String.valueOf(result.getTime(VehicleDB.getEndTime())),
										"-1");
			timings.setWeekDay(result.getString("day"));
			System.out.println(String.valueOf(result.getString(VehicleDB.getWeekDay())));
			vehicle.setTiming(timings);
			
			User user = new UserDao().getUser(result.getString(VehicleDB.getDriverId()), conn);
			vehicle.setDriver(user);
			Location location = new LocationsDao().getLocation(result.getString(VehicleDB.getStartLocationId()), conn);
			vehicle.setStartLocation(location);
			
			location = new LocationsDao().getLocation(result.getString(VehicleDB.getEndLocationId()), conn);
			vehicle.setEndLocation(location);
			
			Route route = new RouteDao().getRoute(result.getString(VehicleDB.getRouteId()), conn);
			vehicle.setRoute(route);
			
			vehicleList.add(vehicle);
			
			System.out.println(vehicle.getTitle());
		}
		
	}catch(Exception e) {
		System.out.println(e.toString());
	}
	System.out.println("vehicle"+vehicle);
	return vehicle;
}


	public VehicleResponse initialiseVehicle(VehiclePayload payload, Connection conn) {
		BaseResponse baseResponse = new BaseResponse();
		VehicleResponse vehicleResponse = new VehicleResponse();
		PreparedStatement roleQuery = null;
		ResultSet result = null;
		
		boolean status = true;
		ArrayList<Vehicle> vehicleList = new ArrayList<>();
		
		System.out.println(payload);
		
		try {
			String query = "select * from " + Constants.tableVehicle +" where " 
					+ VehicleDB.getBusId() + " = " + payload.getVehicleId();
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			int driverId = -99;
			int ownerId = -99;
			while(result.next()) {
				Vehicle vehicle = new Vehicle();
				vehicle.setuId(result.getString(VehicleDB.getBusId()));
				vehicle.setVehicleName(result.getString(VehicleDB.getVehicleName()));
				//System.out.println(result.getString(VehicleDB.getVehicleName()));
				
				Category category = new Category();
				category.setCategoryId(String.valueOf(result.getInt(VehicleDB.getVehicleCategory())));
				vehicle.setCategory(category);
				
				Timing timings = new Timing(String.valueOf(result.getTime(VehicleDB.getStartTime())),
											String.valueOf(result.getTime(VehicleDB.getEndTime())),
											"-1");
				timings.setWeekDay(result.getString("day"));
				System.out.println(String.valueOf(result.getString(VehicleDB.getWeekDay())));
				vehicle.setTiming(timings);
				
				User user = new UserDao().getUser(payload.getDriverId(), conn);
				vehicle.setDriver(user);
				Location location = new LocationsDao().getLocation(result.getString(VehicleDB.getStartLocationId()), conn);
				vehicle.setStartLocation(location);
				
				location = new LocationsDao().getLocation(result.getString(VehicleDB.getEndLocationId()), conn);
				vehicle.setEndLocation(location);
				
				Route route = new RouteDao().getRoute(result.getString(VehicleDB.getRouteId()), conn,true);
				vehicle.setRoute(route);
				List<Route> s = route.getSubRoute();
				vehicle.setSub(s);
				System.out.println(route.getSubRoute().size());
				vehicleList.add(vehicle);
			}
			
		}catch(Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		if(status) {
			baseResponse.setStatusCode(Constants.SU200); 
			baseResponse.setMessage(Constants.LOCATION_RETRIVED_SUCESSFULLY);
			vehicleResponse.setBaseResponse(baseResponse);
			vehicleResponse.setVehicleList(vehicleList);
			if(new LiveStatusDao().setLiveBus(vehicleList.get(0),conn)) {
				return vehicleResponse;
			}else {
				baseResponse.setStatusCode(Constants.SU500);
				baseResponse.setMessage(Constants.SERVER_ERROR);
				vehicleResponse.setBaseResponse(baseResponse);
				vehicleResponse.setVehicleList(null);
				return vehicleResponse;
			}
		}
		else {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			vehicleResponse.setBaseResponse(baseResponse);
		}
		return vehicleResponse;
	}
	
	private String makeQuery(VehiclePayload payload,Connection conn) {
		
		Timing timing = payload.getTiming();
		String startTime = timing.getStartTime();
		String startLocation = payload.getStartLocation();
		String endLocation = payload.getEndLocation();
		String query = "select * from " + Constants.tableVehicle +" where " 
						+ VehicleDB.getWeekDay() + " = " + timing.getWeekDay()
						+ " ORDER BY " + VehicleDB.getStartTime() 
						+  " LIMIT " + String.valueOf(payload.getStart()) + "," + String.valueOf(payload.getLimit()) ;
		
		String startLocationId = new LocationsDao().getLocation(startLocation, 0, conn);
		String endLocationId = new LocationsDao().getLocation(endLocation, 1, conn);
		
		if(timing.getStartTime() != null && !timing.getStartTime().equals("")) {
		query = "select * from " + Constants.tableVehicle + " where "
				+ VehicleDB.getStartTime() + " >= " + startTime  + " and "
				+ VehicleDB.getWeekDay() + " = " + timing.getWeekDay()
				+ " ORDER BY " + VehicleDB.getStartTime() 
				+ " LIMIT " + String.valueOf(payload.getStart()) + "," + String.valueOf(payload.getLimit());
		}
		if(startLocation != null && !startLocation.equals("-99") && !payload.getStartLocation().equals("")) {
		query = "select * from " + Constants.tableVehicle + " where " 
				+ VehicleDB.getStartLocationId() + " = " + startLocationId +" and "
				+ VehicleDB.getStartTime() + " >= " + startTime + " and "
				+ VehicleDB.getWeekDay() + " = " + timing.getWeekDay()
				+ " ORDER BY " + VehicleDB.getStartTime() 
				+ " LIMIT " + String.valueOf(payload.getStart()) + "," + String.valueOf(payload.getLimit());  
		}
		if(endLocation != null && !endLocation.equals("-99") && !payload.getEndLocation().equals("")) {
			query = "select * from " + Constants.tableVehicle + " where " 
					+ VehicleDB.getStartLocationId() + " = " + startLocationId +" and " 
					+ VehicleDB.getEndLocationId() + " = " + endLocationId + " and "
					+ VehicleDB.getStartTime() + " >= " + startTime + " and "
					+ VehicleDB.getWeekDay() + " = " + timing.getWeekDay()
					+ " ORDER BY " + VehicleDB.getStartTime() 
					+ " LIMIT " + String.valueOf(payload.getStart()) + "," + String.valueOf(payload.getLimit());  
		}
		return query;
	}


	public VehicleResponse createVehicle(VehiclePayload payload, Connection conn) {

		Vehicle vehicle = payload.getVehicle();
		BaseResponse baseResponse = new BaseResponse();
		
		PreparedStatement roleQuery = null;
		ResultSet genratedTuple;
		int result = -99;
		VehicleResponse vehicleResponse = new VehicleResponse();
		vehicleResponse.setBaseResponse(baseResponse);
		vehicleResponse.setVehicle(vehicle);
		boolean status = true;
		
		Location startLocation = vehicle.getStartLocation();
		Location endLocation = vehicle.getEndLocation();
		
		String startLocationId = new LocationsDao().getLocation(startLocation.getTitle(), 0, conn);
		System.out.println(startLocationId);
		if(startLocationId.equals("-99") ) {
			startLocationId = new LocationsDao().createLocation(startLocation.getTitle(),startLocation.getLatitude(),startLocation.getLongitude(), conn);
		}
		String endLocationId = new LocationsDao().getLocation(endLocation.getTitle(), 1, conn);
		if(endLocationId.equals("-99") ) {
			endLocationId = new LocationsDao().createLocation(endLocation.getTitle(),endLocation.getLatitude(),endLocation.getLongitude(), conn);
		}
		System.out.println(endLocationId);

		String routeId = new RouteDao().getRoute(startLocationId,endLocationId, conn);
		if(routeId.equals("-99")) {
			System.out.println("damnnn");

			routeId = new RouteDao().createRoute(startLocationId,endLocationId, conn);
		}
		System.out.println(routeId);

		try {
			System.out.println("mannnn");

			String query = "INSERT into"
					       + Constants.tableVehicle
					       + "(" + VehicleDB.getPlateId()
					       + "," + VehicleDB.getOwnerId()
					       + "," + VehicleDB.getRouteId()
					       + "," + VehicleDB.getDriverId()
					       + "," + VehicleDB.getVehicleName()
					       + "," + VehicleDB.getVehicleCategory()
					       + "," + VehicleDB.getWeekDay()
					       + "," + VehicleDB.getStartTime()
					       + "," + VehicleDB.getEndTime()
					       + "," + VehicleDB.getStartLocationId()
					       + "," + VehicleDB.getEndLocationId()+")"
					       + " values "
					       + "('" + vehicle.getVehicleRegNum()
					       + "'," + "1"
					       + "," + routeId
					       + "," + "1"
					       + ",'" + vehicle.getVehicleName()
					       + "'," + vehicle.getCategory().getCategoryId()
					       + ",'" + vehicle.getTiming().getStartDate()
					       + "','" + vehicle.getTiming().getStartTime()
					       + "','" + vehicle.getTiming().getEndTime()
					       + "',"  + Integer.parseInt(startLocationId) 
					       + " ,"  + Integer.parseInt(endLocationId) +")";
			System.out.println(query);
			roleQuery = conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			result = roleQuery.executeUpdate();
			if(result>0){
				genratedTuple = roleQuery.getGeneratedKeys();
				while (genratedTuple.next()) {
					vehicle.setuId(genratedTuple.getString(1));
				}
			}
			
		}catch(Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		if(status) {
			baseResponse.setStatusCode(Constants.SU200);
			baseResponse.setMessage(Constants.LOCATION_INSERTED_SUCESSFULLY);
			vehicleResponse.setBaseResponse(baseResponse);
			vehicleResponse.setVehicle(vehicle);
		}else {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			vehicleResponse.setBaseResponse(baseResponse);
		}
		return vehicleResponse;
	}
	
	
	

}
