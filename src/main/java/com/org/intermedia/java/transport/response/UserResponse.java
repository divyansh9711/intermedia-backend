package com.org.intermedia.java.transport.response;
import java.util.ArrayList;

import com.org.intermedia.java.transport.models.User;

public class UserResponse {
	private BaseResponse baseResponse;
	private User user;
	private ArrayList<User> userList;
	public BaseResponse getBaseResponse() {
		return baseResponse;
	}
	public void setBaseResponse(BaseResponse baseResponse) {
		this.baseResponse = baseResponse;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public ArrayList<User> getUserList() {
		return userList;
	}
	public void setUserList(ArrayList<User> userList) {
		this.userList = userList;
	}
}