package com.org.intermedia.java.transport.response;
import java.util.ArrayList;

import com.org.intermedia.java.transport.models.Route;

public class RouteResponse {

	private BaseResponse baseResponse;
	private Route route;
	private ArrayList<Route> routeList;
	public BaseResponse getBaseResponse() {
		return baseResponse;
	}
	public void setBaseResponse(BaseResponse baseResponse) {
		this.baseResponse = baseResponse;
	}
	public Route getRoute() {
		return route;
	}
	public void setRoute(Route route) {
		this.route = route;
	}
	public ArrayList<Route> getRouteList() {
		return routeList;
	}
	public void setRouteList(ArrayList<Route> routeList) {
		this.routeList = routeList;
	}
}