package com.org.intermedia.java.transport.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.org.intermedia.java.transport.payloads.EmergencyPayload;
import com.org.intermedia.java.transport.payloads.LocationPayload;
import com.org.intermedia.java.transport.payloads.VehiclePayload;
import com.org.intermedia.java.transport.response.EmergencyResponse;
import com.org.intermedia.java.transport.response.LocationResponse;
import com.org.intermedia.java.transport.services.LiveStatusService;
import com.org.intermedia.java.transport.services.LocationService;
import com.org.intermedia.java.transport.utils.SocketServer;

@Path("/liveStatus")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)


public class LiveStatusResource{
	@GET
	@Path("getStatus")
	public Response getStatus() throws Exception{
		Response response = Response.status(200).
                	entity(new LiveStatusService().getEmergency()).
                	header("Access-Control-Allow-Origin", "*").build();
		return response ;
	}
	
	
	@POST
	@Path("emergency")
	public Response setEmergencyStatus(EmergencyPayload payload) throws Exception{
		System.out.println("reache here");
		Response response = Response.status(200)
				.entity(new LiveStatusService().setEmergencyStatus(payload))
				.header("Access-Control-Allow-Origin", "*")
				.build();
		System.out.println("response sent" + response);
		return response; 
	}
	@GET
	@Path("getAlerted")
	public EmergencyResponse getAlerted() throws Exception{
		return new LiveStatusService().getAlerted(); 
	}
	
	@POST
	@Path("setAlert")
	public EmergencyResponse setAlert(EmergencyPayload payload) throws Exception{
		return new LiveStatusService().setAlert(payload); 
	}
	
	@POST
	@Path("resolve")
	public EmergencyResponse resolve(EmergencyPayload payload) throws Exception{
		return new LiveStatusService().resolve(payload); 
	}
	
	
}
