package com.org.intermedia.java.transport.utils;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.org.intermedia.java.transport.payloads.EmergencyPayload;
import com.org.intermedia.java.transport.response.EmergencyResponse;

import java.util.*;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkHTTPRequest {
	private final OkHttpClient httpClient = new OkHttpClient();

//    public static void main(String[] args) throws Exception {
//
//        OkHttpExample obj = new OkHttpExample();
//
//        System.out.println("Testing 1 - Send Http GET request");
//        obj.sendGet();
//
//        System.out.println("Testing 2 - Send Http POST request");
//        obj.sendPost();
//
//    }

    private void sendGet() throws Exception {

//        Request request = new Request.Builder()
//                .url("http://www.localhost:8000/")
//                .addHeader("custom-key", "mkyong")  // add request headers
//                .addHeader("User-Agent", "OkHttp Bot")
//                .build();
//
//        try (Response response = httpClient.newCall(request).execute()) {
//
//            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
//
//            // Get response body
//            System.out.println(response.body().string());
//        }

    }
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public String sendEmergencyPost(EmergencyResponse payload) throws Exception {
        // form parameters
    	ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
    	String json = ow.writeValueAsString(payload);
    	 @SuppressWarnings("deprecation")
		RequestBody formBody = RequestBody.create(JSON,json);
    	 System.out.println(json);
    	 System.out.println(formBody);
    	 System.out.println("SENDING request" + formBody);
        Request request = new Request.Builder()
                .url("http://localhost:8000/emergency")
                .post(formBody)
                .build();
        
        try (Response response = httpClient.newCall(request).execute()) {

            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

            // Get response body
            System.out.println(response.body().string());
            return response.body().toString();
        }catch(Exception e) {}
        return "";
    }
    


}
