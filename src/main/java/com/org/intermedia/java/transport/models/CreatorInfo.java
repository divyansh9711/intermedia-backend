package com.org.intermedia.java.transport.models;

public class CreatorInfo {

	private Timing createTime;
	private String creatorId;
	private String creatorName;
	public Timing getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timing createTime) {
		this.createTime = createTime;
	}
	public String getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	
	
	
}
