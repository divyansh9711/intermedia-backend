package com.org.intermedia.java.transport.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.org.intermedia.java.transport.payloads.EmergencyPayload;
import com.org.intermedia.java.transport.payloads.VehiclePayload;
import com.org.intermedia.java.transport.response.EmergencyResponse;
import com.org.intermedia.java.transport.response.VehicleResponse;
import com.org.intermedia.java.transport.services.InitialiseVehicleService;
import com.org.intermedia.java.transport.services.LiveStatusService;

@Path("/init")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class InitailiseVehicleResource {

	@POST
	@Path("/spawn")
	public VehicleResponse intialise(VehiclePayload payload) throws Exception{
		return new InitialiseVehicleService().getVehicle(payload);
	}
	
	@POST
	@Path("/kill")
	public VehicleResponse stop(VehiclePayload payload) throws Exception{
		
		return new InitialiseVehicleService().stop(payload);
	}
	
	
}
