package com.org.intermedia.java.transport.services;

import java.sql.Connection;

import com.org.intermedia.java.transport.dao.LiveStatusDao;
import com.org.intermedia.java.transport.dao.VehicleDao;
import com.org.intermedia.java.transport.payloads.InitialiseVehiclePayload;
import com.org.intermedia.java.transport.payloads.VehiclePayload;
import com.org.intermedia.java.transport.response.VehicleResponse;

public class InitialiseVehicleService {

	public VehicleResponse getVehicle(VehiclePayload payload) throws Exception {
		Connection conn = getConnection();
		return new VehicleDao().initialiseVehicle(payload,conn);
	}
	
	public VehicleResponse stop(VehiclePayload payload) throws Exception {
		Connection conn = getConnection();
		return new LiveStatusDao().deleteLiveBus(payload.getVehicleId(),conn);
	}
	
	private Connection getConnection() throws Exception {
		DbConnection dbConnection=new DbConnection();
		Connection connection = dbConnection.getConnection();
		return connection;
	}
}
