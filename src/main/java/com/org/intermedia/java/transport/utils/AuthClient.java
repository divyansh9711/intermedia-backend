package com.org.intermedia.java.transport.utils;

import java.math.BigInteger;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class AuthClient {
	private String apiKey = "Bar12345Bar12345";

	public boolean auth(String encKey, String payload) {

		String key = decrypt(encKey);

		if (!key.equals("-99") && !payload.equals("") && !getSHA(payload).equals("") && getSHA(payload).equals(key)) {
			System.out.println("it think it worked");
			return true;
		}
		return false;
	}

	public String getSHA(String input) {

		try {

			// Static getInstance method is called with hashing SHA
			MessageDigest md = MessageDigest.getInstance("SHA-384");

			// digest() method called
			// to calculate message digest of an input
			// and return array of byte
			byte[] messageDigest = md.digest(input.getBytes());

			// Convert byte array into signum representation
			BigInteger no = new BigInteger(1, messageDigest);

			// Convert message digest into hex value
			String hashtext = no.toString(16);

			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "invalid";
	}

	public String decrypt(String text) {
		// String text = "hello";
		try {
			Key aesKey = new SecretKeySpec(Arrays.copyOf(apiKey.getBytes("UTF-8"), 16), "AES");
			Cipher cipher = Cipher.getInstance("AES");
//			 
//			  cipher.init(Cipher.ENCRYPT_MODE, aesKey); byte[] encrypted =
//			  cipher.doFinal(text.getBytes("UTF-8")); String enc =
//			  DatatypeConverter.printBase64Binary(encrypted); System.out.println(enc);
//			 
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			String decrypted = new String(cipher.doFinal(DatatypeConverter.parseBase64Binary(text)));
			System.out.println("decrypted : \n\n\n" + decrypted);
			return decrypted;
		} catch (Exception e) {
			System.out.println("nonononono");
			e.printStackTrace();
		}
		return "-99";
	}

}
