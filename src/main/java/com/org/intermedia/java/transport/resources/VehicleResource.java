package com.org.intermedia.java.transport.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.org.intermedia.java.transport.payloads.VehiclePayload;
import com.org.intermedia.java.transport.response.VehicleResponse;
import com.org.intermedia.java.transport.services.VehicleService;

@Path("/vehicle")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VehicleResource {

	@POST
	@Path("getVehicle")
	public VehicleResponse getVehicle(VehiclePayload payload) throws Exception {
		return new VehicleService().getVehicle(payload);
	}
	@POST
	@Path("createVehicle")
	public VehicleResponse createVehicle(VehiclePayload payload) throws Exception {
		System.out.println("looool");
		return new VehicleService().createVehicle(payload);
	}
	@POST
	@Path("get")
	public VehicleResponse get(VehiclePayload payload) throws Exception {
		return new VehicleService().get(payload);
	}
	
}