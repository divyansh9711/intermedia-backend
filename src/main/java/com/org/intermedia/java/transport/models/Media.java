package com.org.intermedia.java.transport.models;

import java.util.List;
/**
 * @author Divyansh Singh
 *
 */
public class Media {
	private List<Media> mediaList;
	private String mediaUrl;
	private List<String> mediaUrls;
	private String mediaInfo;
	private String mediaId;
	private String mediaTitle;
	private Category mediaCategory;
	public List<Media> getMediaList() {
		return mediaList;
	}
	public void setMediaList(List<Media> mediaList) {
		this.mediaList = mediaList;
	}
	public String getMediaUrl() {
		return mediaUrl;
	}
	public void setMediaUrl(String mediaUrl) {
		this.mediaUrl = mediaUrl;
	}
	public List<String> getMediaUrls() {
		return mediaUrls;
	}
	public void setMediaUrls(List<String> mediaUrls) {
		this.mediaUrls = mediaUrls;
	}
	public String getMediaInfo() {
		return mediaInfo;
	}
	public void setMediaInfo(String mediaInfo) {
		this.mediaInfo = mediaInfo;
	}
	public String getMediaId() {
		return mediaId;
	}
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	public String getMediaTitle() {
		return mediaTitle;
	}
	public void setMediaTitle(String mediaTitle) {
		this.mediaTitle = mediaTitle;
	}
	public Category getMediaCategory() {
		return mediaCategory;
	}
	public void setMediaCategory(Category mediaCategory) {
		this.mediaCategory = mediaCategory;
	}
	
	
}