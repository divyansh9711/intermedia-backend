package com.org.intermedia.java.transport.response;

import java.util.ArrayList;

import com.org.intermedia.java.transport.models.Vehicle;

public class EmergencyResponse {
	private String uId;
	private String message;
	private BaseResponse baseResponse;
	private ArrayList<Vehicle> vehicleList;
	private ArrayList<String> reportedId;
	
	
	public EmergencyResponse() {
		vehicleList = new ArrayList<Vehicle>();
		reportedId = new ArrayList<String>();
	}
	public ArrayList<String> getReportedId() {
		return reportedId;
	}
	void setReportedId(ArrayList<String> reportedId) {
		this.reportedId = reportedId;
	}
	public ArrayList<Vehicle> getVehicleList() {
		return vehicleList;
	}

	public void setVehicleList(ArrayList<Vehicle> vehicleList) {
		this.vehicleList = vehicleList;
	}

	public BaseResponse getBaseResponse() {
		return baseResponse;
	}

	public void setBaseResponse(BaseResponse baseResponse) {
		this.baseResponse = baseResponse;
	}


	public String getuId() {
		return uId;
	}

	public void setuId(String uId) {
		this.uId = uId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
