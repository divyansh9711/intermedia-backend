package com.org.intermedia.java.transport.services;

import java.sql.Connection;

import com.google.gson.Gson;
import com.org.intermedia.java.transport.dao.LocationsDao;
import com.org.intermedia.java.transport.dao.UserDao;
import com.org.intermedia.java.transport.payloads.LocationPayload;
import com.org.intermedia.java.transport.payloads.UserPayload;
import com.org.intermedia.java.transport.response.BaseResponse;
import com.org.intermedia.java.transport.response.LocationResponse;
import com.org.intermedia.java.transport.response.UserResponse;
import com.org.intermedia.java.transport.utils.AuthClient;
import com.org.intermedia.java.transport.utils.Constants;

public class UserService {
	
	public UserResponse createUser(UserPayload payload) throws Exception{
		if(checkPayload(payload)) {
			BaseResponse baseResponse = new BaseResponse();
			UserResponse userResponse = new UserResponse();
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			userResponse.setBaseResponse(baseResponse);
			return userResponse;
		}
		Connection connection = getConnection();
		UserDao userDao = new UserDao();
		return userDao.createUser(payload, connection);
	}
	private Connection getConnection() throws Exception {
		DbConnection dbConnection=new DbConnection();
		Connection connection = dbConnection.getConnection();
		return connection;
	}
	public UserResponse getUser(UserPayload payload) throws Exception{
		if(checkPayload(payload) || !authClient(payload)) {
			BaseResponse baseResponse = new BaseResponse();
			UserResponse userResponse = new UserResponse();
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			userResponse.setBaseResponse(baseResponse);
			return userResponse;
		}
		
		Connection connection = getConnection();
		UserDao userDao = new UserDao();
		return userDao.authUser(payload, connection);
	}
	public boolean checkPayload(UserPayload payload) {
		if(payload == null ) {
			return true;
		}else if(payload.getUser() != null && (payload.getUser().getFirstName() == null || 
				 payload.getUser().getLastName() == null ||
				 payload.getUser().getUserEmail() == null)) {
			return true;
		}else if(payload.getUser() != null && (payload.getUser().getFirstName().equals("") || 
				 payload.getUser().getLastName().equals("") ||
				 payload.getUser().getUserEmail().equals(""))) {
			return true;
		}else if(payload.getUserKey() == null) {return true;}
		return false;
	}
	public boolean authClient(UserPayload payload) {
		
		String apiKey = payload.getAPIKey();
		String json = new Gson().toJson(payload.getUserKey());
		System.out.println(apiKey);
		System.out.println(json);
		
		return new AuthClient().auth(apiKey,json);
	}
	
	
}
