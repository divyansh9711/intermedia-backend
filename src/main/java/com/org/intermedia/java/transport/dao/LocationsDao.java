package com.org.intermedia.java.transport.dao;

import java.sql.*;
import java.util.ArrayList;

import com.org.intermedia.java.transport.models.Category;
import com.org.intermedia.java.transport.models.Location;
import com.org.intermedia.java.transport.payloads.LocationPayload;
import com.org.intermedia.java.transport.response.BaseResponse;
import com.org.intermedia.java.transport.response.LocationResponse;
import com.org.intermedia.java.transport.tables.LocationDB;
import com.org.intermedia.java.transport.utils.Constants;

public class LocationsDao {
	
	public LocationResponse getLocation(LocationPayload payload,Connection conn) {
		
		BaseResponse baseResponse = new BaseResponse();
		String locationId = payload.getLocationId();
		
		LocationResponse locationResponse = new LocationResponse();
		PreparedStatement roleQuery = null;
		ResultSet result = null;
		Location location = new Location();
		ArrayList<Location> locationList = new ArrayList<Location>();
		locationResponse.setLocation(location);
		boolean status = true;
		try {
			String query = "select * from " + Constants.tableLocation + " where " + "locationId=" + locationId;  
			//System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			
			while(result.next()) {
				location.setuId(locationId);
				location.setCountry(result.getString("country"));
				location.setInfo(result.getString("info"));
				location.setLatitude(result.getString("latitude"));
				location.setLocality(result.getString("locality"));
				location.setLongitude(result.getString("longitude"));
				location.setTitle(result.getString("title"));
				location.setZipCode(result.getString("zipCode"));
				locationList.add(location);
				locationResponse.setLocation(location);

			}
			locationResponse.setLocationList(locationList);
			
			
		}catch(Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		if(status) {
			baseResponse.setStatusCode(Constants.SU200); 
			baseResponse.setMessage(Constants.LOCATION_RETRIVED_SUCESSFULLY);
			locationResponse.setBaseResponse(baseResponse);
		}
		else {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			locationResponse.setBaseResponse(baseResponse);
		}
		return locationResponse;
	}
	
	public Location getLocation(String locationId,Connection conn) {
		PreparedStatement roleQuery = null;
		ResultSet result = null;
		Location location = new Location();
		try {
			String query = "select * from " + "location" + " where " + "locationId=" + locationId;  
			//System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			
			while(result.next()) {
				location.setuId(locationId);
				location.setCountry(result.getString("country"));
				location.setInfo(result.getString("info"));
				location.setLatitude(result.getString("latitude"));
				location.setLocality(result.getString("locality"));
				location.setLongitude(result.getString("longitude"));
				location.setTitle(result.getString("title"));
				location.setZipCode(result.getString("zipCode"));

			}
			return location;
		}catch(Exception e) {
			System.out.println(e.toString());
		}
		return location;
	}
	
	public String getLocation(String locationTitle,int x,Connection conn) {
		PreparedStatement roleQuery = null;
		ResultSet result = null;
		String location = "-99";
		try {
			String query = "select * from " + "location" + " where " + LocationDB.getTitle() +"= '" + locationTitle + "'";  
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			
			while(result.next()) {
				location = String.valueOf(result.getInt(LocationDB.getLocationId()));
			}
			return location;
		}catch(Exception e) {
			System.out.println(e.toString());
		}
		return location;
	}

	public LocationResponse createLocation(LocationPayload lPayload,Connection conn) {
		Location location = new Location();
		Category category = new Category();
		category.setCategoryId("648");
		category.setCategoryTitle("default");
		location = lPayload.getLocation();
		
		PreparedStatement roleQuery = null;
		int result = -99;
		ResultSet genratedTuple;
		
		BaseResponse baseResponse = new BaseResponse();
		LocationResponse locationResponse = new LocationResponse();
		locationResponse.setBaseResponse(baseResponse);
		locationResponse.setLocation(location);
		boolean status = true;
		
		try {
			String query = "INSERT into " 
						 + Constants.tableLocation
						 + "(" + LocationDB.getCity()
						 + "," + LocationDB.getCountry()
						 + "," + LocationDB.getInfo()
						 + "," + LocationDB.getLatitude()
						 + "," + LocationDB.getLongitude()
						 + "," + LocationDB.getLocality()
						 + "," + LocationDB.getState()
						 + "," + LocationDB.getTitle()
						 + "," + LocationDB.getZipCode() + ")"
						 + " values "
						 + "('" + location.getCity()
						 + "','" + location.getCountry()
						 + "','" + location.getInfo()
						 + "','" + location.getLatitude()
						 + "','" + location.getLongitude()
						 + "','" + location.getLocality()
						 + "','" + location.getState()
						 + "','" + location.getTitle()
						 + "','" + location.getZipCode() + "')";
			
			//System.out.println(query);
			roleQuery = conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			result = roleQuery.executeUpdate();
			
			if(result>0){
				genratedTuple = roleQuery.getGeneratedKeys();
				while (genratedTuple.next()) {
					location.setuId(genratedTuple.getString(1));
				}
			}
			
			
		}catch(Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		if(status) {
			baseResponse.setStatusCode(Constants.SU200);
			baseResponse.setMessage(Constants.LOCATION_INSERTED_SUCESSFULLY);
			locationResponse.setBaseResponse(baseResponse);
			locationResponse.setLocation(location);
		}else {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			locationResponse.setBaseResponse(baseResponse);
		}
		return locationResponse;
	}
	public String createLocation(String locationTitle,String lat,String lon,Connection conn) {
		Location location = new Location();
		Category category = new Category();
		category.setCategoryId("648");
		category.setCategoryTitle("default");
		
		String locationId = "-99";
		
		location.setTitle(locationTitle);
		location.setLatitude(lat);
		location.setLongitude(lon);
		
		PreparedStatement roleQuery = null;
		int result = -99;
		ResultSet genratedTuple;
		
		boolean status = true;
		
		try {
			String query = "INSERT into " 
						 + Constants.tableLocation
						 + "(" + LocationDB.getLatitude()
						 + "," + LocationDB.getLongitude()
						 + "," + LocationDB.getTitle() + ")"
						 + " values "
						 + "('"  + location.getLatitude()
						 + "','" + location.getLongitude()
						 + "','" + location.getTitle() + "')";
			
			System.out.println(query);
			roleQuery = conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			result = roleQuery.executeUpdate();
			
			if(result>0){
				genratedTuple = roleQuery.getGeneratedKeys();
				while (genratedTuple.next()) {
					locationId = genratedTuple.getString(1);
				}
			}
		}catch(Exception e) {
			System.out.println(e.toString());
		}
		return locationId;
	}
}
