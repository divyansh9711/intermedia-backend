package com.org.intermedia.java.transport.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.org.intermedia.java.transport.models.Category;
import com.org.intermedia.java.transport.models.Vehicle;
import com.org.intermedia.java.transport.payloads.EmergencyPayload;
import com.org.intermedia.java.transport.payloads.VehiclePayload;
import com.org.intermedia.java.transport.response.BaseResponse;
import com.org.intermedia.java.transport.response.EmergencyResponse;
import com.org.intermedia.java.transport.response.VehicleResponse;
import com.org.intermedia.java.transport.tables.LiveStatusDB;
import com.org.intermedia.java.transport.tables.UserDB;
import com.org.intermedia.java.transport.tables.VehicleDB;
import com.org.intermedia.java.transport.utils.Constants;

public class LiveStatusDao {

	public boolean setLiveBus(Vehicle vehicle, Connection conn) {
		PreparedStatement roleQuery = null;
		ResultSet genratedTuple;
		int result = -99;
		boolean status = true;
		try {
			String query = "INSERT into"
				       + Constants.tableLiveStatus
				       + "(" + LiveStatusDB.getDriverId()
				       + "," + LiveStatusDB.getBusId()
				       + "," + LiveStatusDB.getStatus()
				       + "," + LiveStatusDB.getLatitude()
				       + "," + LiveStatusDB.getLongitude()+ ")"
				       + " values "
				       + "('" + vehicle.getDriver().getuId()
				       + "'," + vehicle.getuId()
				       + "," + "0"
				       + "," + vehicle.getStartLocation().getLatitude()
				       + "," + vehicle.getStartLocation().getLongitude()
				       +")";
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeUpdate();
		}catch (Exception e) {
			System.out.println(e.toString());
			return false;
		}
		return true;
	}
	
	public VehicleResponse deleteLiveBus(String id, Connection conn) {
		PreparedStatement roleQuery = null;
		ResultSet genratedTuple;
		BaseResponse baseResponse = new BaseResponse();
		VehicleResponse vehicleResponse = new VehicleResponse();
		int result = -99;
		boolean status = true;
		try {
			String query = "DELETE FROM"
				       + Constants.tableLiveStatus
				       + " where "
				       + LiveStatusDB.getBusId() 
				       + " = "
				       + id;
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeUpdate();
		}catch (Exception e) {
			System.out.println(e.toString());
			status =  false;
			
		}
		if(status) {
			baseResponse.setStatusCode(Constants.SU200); 
			baseResponse.setMessage(Constants.LOCATION_RETRIVED_SUCESSFULLY);
			vehicleResponse.setBaseResponse(baseResponse);
		}
		else {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			vehicleResponse.setBaseResponse(baseResponse);
		}
		return vehicleResponse;
		
	}
	
	
	public EmergencyResponse setEmergencyStatus(EmergencyPayload payload,Connection conn) {
		
		String id = payload.getVehicleId();
		PreparedStatement roleQuery = null;
		ResultSet genratedTuple;
		int result = -99;
		BaseResponse baseResponse = new BaseResponse();
		EmergencyResponse response = new EmergencyResponse();
		response.setuId(id);
		
		boolean status = true;
		
		try {
			String query = "UPDATE" 
							+ Constants.tableLiveStatus + " SET "
							+ LiveStatusDB.getStatus() + " = "
							+ payload.getStatus() + "," 
							+ LiveStatusDB.getMessage() + " = '" 
							+ payload.getMessage()+"' WHERE "
							+ LiveStatusDB.getBusId() + " = "
							+ id;

			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeUpdate();
		}catch (Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		if(!status) {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			response.setBaseResponse(baseResponse);
			return response;
		}
		baseResponse.setStatusCode(Constants.SU200);
		baseResponse.setMessage("Successfull");
		response.setBaseResponse(baseResponse);
		response.setMessage(payload.getMessage());
		return response;
	}
	
public EmergencyResponse setAlert(EmergencyPayload payload,Connection conn) {
		
		String id = payload.getVehicleId();
		PreparedStatement roleQuery = null;
		ResultSet genratedTuple;
		int result = -99;
		BaseResponse baseResponse = new BaseResponse();
		EmergencyResponse response = new EmergencyResponse();
		response.setuId(id);
		
		boolean status = true;
		
		try {
			String query = "UPDATE" 
							+ Constants.tableLiveStatus + " SET "
							+ LiveStatusDB.getAlertStatus() + " = "
							+ payload.getAlertStatus() + " WHERE "
							+ LiveStatusDB.getBusId() + " = "
							+ id;

			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeUpdate();
		}catch (Exception e) {
			System.out.println(e.toString());
			status = false;
		}
		if(!status) {
			baseResponse.setStatusCode(Constants.SU500);
			baseResponse.setMessage(Constants.SERVER_ERROR);
			response.setBaseResponse(baseResponse);
			return response;
		}
		baseResponse.setStatusCode(Constants.SU200);
		baseResponse.setMessage("Successfull");
		response.setBaseResponse(baseResponse);
		response.setMessage("Emergency Reported");
		return response;
	}

public EmergencyResponse resolve(EmergencyPayload payload,Connection conn) {
	
	String id = payload.getVehicleId();
	PreparedStatement roleQuery = null;
	ResultSet genratedTuple;
	int result = -99;
	BaseResponse baseResponse = new BaseResponse();
	EmergencyResponse response = new EmergencyResponse();
	response.setuId(id);
	
	boolean status = true;
	
	try {
		String query = "UPDATE" 
						+ Constants.tableLiveStatus + " SET "
						+ LiveStatusDB.getAlertStatus() + " = "
						+ "0" +" , " + LiveStatusDB.getStatus() + " = 0, " + LiveStatusDB.getMessage() + " = 'resolved' " + " WHERE "
						+ LiveStatusDB.getBusId() + " = "
						+ id;

		System.out.println(query);
		roleQuery = conn.prepareStatement(query);
		result = roleQuery.executeUpdate();
	}catch (Exception e) {
		System.out.println(e.toString());
		status = false;
	}
	if(!status) {
		baseResponse.setStatusCode(Constants.SU500);
		baseResponse.setMessage(Constants.SERVER_ERROR);
		response.setBaseResponse(baseResponse);
		return response;
	}
	baseResponse.setStatusCode(Constants.SU200);
	baseResponse.setMessage("Successfull");
	response.setBaseResponse(baseResponse);
	response.setMessage("Emergency Reported");
	return response;
}
	
	public EmergencyResponse getEmergency(Connection conn) {
		PreparedStatement roleQuery = null;
		ResultSet result = null;
		BaseResponse baseResponse = new BaseResponse();
		EmergencyResponse response = new EmergencyResponse();
		
		baseResponse.setStatusCode(Constants.SU900);
		boolean status = true;
		try {
			String query = "SELECT * FROM " + Constants.tableLiveStatus + " where " + LiveStatusDB.getStatus() + " = " + "99";
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			
			while(result.next()) {
				String id = String.valueOf(result.getInt(LiveStatusDB.getBusId()));
				if(id!= null) {
					response.getReportedId().add(id);
				}
				response.setMessage("Suspicious");
				baseResponse.setStatusCode(Constants.SU999);
			}
			
		}catch (Exception e) {
			System.out.println(e.toString());
			baseResponse.setStatusCode(Constants.SU500);
			status = false;
		}
		response.setBaseResponse(baseResponse);
		return response;
		
	}
	public EmergencyResponse getAlerted(Connection conn) {
		PreparedStatement roleQuery = null;
		ResultSet result = null;
		BaseResponse baseResponse = new BaseResponse();
		EmergencyResponse response = new EmergencyResponse();
		baseResponse.setStatusCode(Constants.SU900);
		ArrayList<Vehicle> vehicleList = new ArrayList<Vehicle>();
		String id = "";
		
		boolean status = true;
		try {
			String query = "SELECT * FROM " + Constants.tableLiveStatus + " where " + LiveStatusDB.getAlertStatus() + " = " + "1";
			System.out.println(query);
			roleQuery = conn.prepareStatement(query);
			result = roleQuery.executeQuery();
			
			while(result.next()) {
				
				id = (String.valueOf(result.getInt(LiveStatusDB.getBusId())));
				if(!id.equals("")) {
					
					Vehicle vehicle = new VehicleDao().get(id,conn);
					if(vehicle != null) {
						response.getVehicleList().add(vehicle);
					}
				}
			}
			
		}catch (Exception e) {
			System.out.println(e.toString());
			baseResponse.setStatusCode(Constants.SU500);
			status = false;
		}
		response.setBaseResponse(baseResponse);
		return response;
		
	}
}
