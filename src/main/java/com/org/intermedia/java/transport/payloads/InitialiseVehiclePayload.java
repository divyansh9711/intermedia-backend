package com.org.intermedia.java.transport.payloads;

public class InitialiseVehiclePayload extends Payload {

	private String intialiseTime;
	private String busId;
	private String driverId;
	public String getIntialiseTime() {
		return intialiseTime;
	}
	public void setIntialiseTime(String intialiseTime) {
		this.intialiseTime = intialiseTime;
	}
	public String getBusId() {
		return busId;
	}
	public void setBusId(String busId) {
		this.busId = busId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	
		
}
