package com.org.intermedia.java.transport.tables;

public class RouteRelationDB {

	public static String tableName = "route_route";
	public static String parentRouteId = "parentRouteId";
	public static String subRouteId = "subRouteId";
	public static String getTableName() {
		return tableName;
	}
	public static void setTableName(String tableName) {
		RouteRelationDB.tableName = tableName;
	}
	public static String getParentRouteId() {
		return parentRouteId;
	}
	public static void setParentRouteId(String parentRouteId) {
		RouteRelationDB.parentRouteId = parentRouteId;
	}
	public static String getSubRouteId() {
		return subRouteId;
	}
	public static void setSubRouteId(String subRouteId) {
		RouteRelationDB.subRouteId = subRouteId;
	}
	
	
	
}
