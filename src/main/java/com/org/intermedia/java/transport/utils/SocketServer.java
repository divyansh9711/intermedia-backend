package com.org.intermedia.java.transport.utils;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.DataListener;

public class SocketServer {
	private Socket          socket   = null; 
    private ServerSocket    server   = null; 
    private ObjectOutputStream out       =  null; 
  
    // constructor with port 
    public SocketServer(int port) 
    { 
        // starts server and waits for a connection 
        try
        { 
            server = new ServerSocket(3002); 
            System.out.println("Server started"); 
  
            System.out.println("Waiting for a client ..."); 
  
            socket = server.accept(); 
            System.out.println("Client accepted"); 
  
            // takes input from the client socket 
            out = new ObjectOutputStream(socket.getOutputStream()); 
  
            String line = ""; 
  
            // reads message from client until "Over" is sent 
           
                try
                { 
                    out.writeObject("x"+"dsfsas"); 
                    System.out.println("writing"); 
  
                } 
                catch(IOException i) 
                { 
                    System.out.println(i); 
                } 
            
            System.out.println("Closing connection"); 
  
            // close connection 
            socket.close(); 
            out.close(); 
        } 
        catch(IOException i) 
        { 
            System.out.println(i); 
        } 
    }
    
//	public SocketServer() {
//		Configuration config = new Configuration();
//	    config.setHostname("localhost");
//	    config.setPort(3002);
//        final SocketIOServer server = new SocketIOServer(config);
//        server.addEventListener("conect", String.class, new DataListener<String>() {
//            @Override
//            public void onData(SocketIOClient client, String data, AckRequest ackRequest) {
//                client.sendEvent("x", "ads");
//            }
//        });
//
//        server.start();
//
//      
//        server.stop();
//        
//	}


}
