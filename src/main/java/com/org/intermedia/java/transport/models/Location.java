package com.org.intermedia.java.transport.models;

/**
 * @author Divyansh Singh
 *
 */
public class Location extends Profile{
	private String latitude;
	private String longitude;
	private String locality;
	private String zipCode;
	private String city;
	private String state;
	private Category locationCategory;  //648: default
	private String country;
	
	public Location() {
		super();
	}
	
	
	
	public Location(String latitude, String longitude, String locality, String zipCode, String city, String state,
			String country) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.locality = locality;
		this.zipCode = zipCode;
		this.city = city;
		this.state = state;
		this.country = country;
	}


	
	public Category getLocationCategory() {
		return locationCategory;
	}



	public void setLocationCategory(Category locationCategory) {
		this.locationCategory = locationCategory;
	}



	public String getState() {
		return state;
	}
	public void setState(String string) {
		state = string;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	
	
}
